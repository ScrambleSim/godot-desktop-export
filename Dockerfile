FROM alpine

# This creates an image which contains headless Godot.
# Export templates for Linux, Mac and Windows are included.

# ==== Example usage ====

# Build image
# sudo docker build -t <image-name> --build-arg GODOT_VERSION=3.2 .

# Run Godot headless
# sudo docker run <image-name> godot --version

# Export to Mac
# sudo docker run <image-name> godot -v --export "Linux" /target/build.zip --path /path/to/project --build-solutions -quit -v

ARG GODOT_VERSION

# Install packages needed for downloading
RUN apk add --no-cache --virtual build-deps \
    ca-certificates \
    wget \
# Install glibc to run Godot
&& wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub \
&& wget -q https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.27-r0/glibc-2.27-r0.apk \
&& apk add glibc-2.27-r0.apk \
&& rm glibc-2.27-r0.apk \
# Directory setup
&& mkdir -p ~/.cache \
&& mkdir -p ~/.config/godot \
&& mkdir -p ~/.local/share/godot/templates/${GODOT_VERSION}.stable \
&& mkdir ~/downloads \
# Download Godot with export templates
&& cd ~/downloads \
&& wget https://downloads.tuxfamily.org/godotengine/${GODOT_VERSION}/Godot_v${GODOT_VERSION}-stable_linux_headless.64.zip -nv \
&& wget https://downloads.tuxfamily.org/godotengine/${GODOT_VERSION}/Godot_v${GODOT_VERSION}-stable_export_templates.tpz -nv \
# Unpack Godot and move to install location
&& unzip Godot_v${GODOT_VERSION}-stable_linux_headless.64.zip \
&& mv Godot_v${GODOT_VERSION}-stable_linux_headless.64 /usr/local/bin/godot \
# Unpack relevant templates and move to install location
&& unzip Godot_v${GODOT_VERSION}-stable_export_templates.tpz \
&& mv templates/*linux_x11_64* templates/*windows_64* templates/*osx* ~/.local/share/godot/templates/${GODOT_VERSION}.stable \
&& rm -rf templates \
# Delete downloads
&& rm -rf ~/downloads/* \
&& apk del build-deps

